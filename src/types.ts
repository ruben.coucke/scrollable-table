export interface BodyContent {
    key: string;
    text: string;
}

export interface IdentityContent {
    key: string;
    expandedText: string;
    shrunkText: string;
    image?: string;
}

export const CELL_HEIGHT = 40;
export const CELL_WIDTH = 130;
export const IDENTITY_COLUMN_MIN_WIDTH = 70;
