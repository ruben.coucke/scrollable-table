import React from 'react';
import { Animated, StyleSheet, View, ViewProps } from 'react-native';
import SheetCell from './SheetCell';
import { CELL_WIDTH, BodyContent, IDENTITY_COLUMN_MIN_WIDTH } from '../types';
import SheetTopLeftCell from './SheetTopLeftCell';

interface SheetHeaderProps extends ViewProps {
    topLeftHeaderText: string;
    headerContent: BodyContent[];
    scrollPositionLeft: Animated.Value;
}

const SheetHeader: React.FC<SheetHeaderProps> = ({topLeftHeaderText, headerContent, scrollPositionLeft}) => {

    return (
        <View style={styles.header}>
            <Animated.View style={[styles.topLeftHeader, {
                transform: [
                    {
                        translateX: scrollPositionLeft.interpolate({
                            inputRange: [0, CELL_WIDTH],
                            outputRange: [0, -CELL_WIDTH + IDENTITY_COLUMN_MIN_WIDTH],
                            extrapolate: 'clamp'
                        })
                    }
                ]
            }
            ]}>
                <SheetTopLeftCell content={topLeftHeaderText} scrollPositionLeft={scrollPositionLeft}/>
            </Animated.View>
            <Animated.View style={[styles.header, {top: -1}, {
                transform: [
                    {
                        translateX: scrollPositionLeft.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, -1]
                        })
                    }
                ]
            }]}>

                {headerContent.map(value => <SheetCell content={value.text} key={value.key}/>)}
            </Animated.View>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {flexDirection: "row", borderTopWidth: 1, borderColor: 'black', backgroundColor: 'white'},
    topLeftHeader: {zIndex: 1, backgroundColor: 'white'}
})

export default SheetHeader;
