import { Animated, Image, StyleSheet, ViewProps } from 'react-native';
import React from 'react';
import { CELL_HEIGHT, CELL_WIDTH, IDENTITY_COLUMN_MIN_WIDTH } from '../types';

interface SheetIdentityCellProps extends ViewProps {
    expandedContent: string;
    scrollPositionLeft: Animated.Value;
    shrunkContent: string;
    imageUrl?: string;
}

const SheetIdentityCell: React.FC<SheetIdentityCellProps> = ({expandedContent, imageUrl, shrunkContent, scrollPositionLeft}) => {
    return (
        <Animated.View style={[styles.cell, styles.centerRow]}>
            <Animated.View style={[styles.centerRow, {
                transform: [
                    {
                        translateX: scrollPositionLeft.interpolate({
                            inputRange: [0, CELL_WIDTH],
                            outputRange: [1, CELL_WIDTH - IDENTITY_COLUMN_MIN_WIDTH + 1],
                            extrapolate: 'clamp'
                        })
                    }
                ]
            }]}>
                {imageUrl ? <Image resizeMethod={'resize'} source={{uri: imageUrl}} style={styles.logo}/> : null}
                <Animated.Text
                    numberOfLines={1}
                    style={[styles.text, {
                        transform: [
                            {
                                scale: scrollPositionLeft.interpolate({
                                    inputRange: [20, 20.1],
                                    outputRange: [1, 0],
                                    extrapolate: 'clamp'
                                })
                            }
                        ]
                    }]}>
                    {expandedContent}
                </Animated.Text>
                <Animated.Text
                    numberOfLines={1}
                    style={[styles.text, {
                        transform: [
                            {
                                scale: scrollPositionLeft.interpolate({
                                    inputRange: [20, 20.1],
                                    outputRange: [0, 1],
                                    extrapolate: 'clamp'
                                })
                            }
                        ]
                    }]}>
                    {shrunkContent}
                </Animated.Text>
            </Animated.View>
        </Animated.View>
    )
};

const styles = StyleSheet.create({
    cell: {
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'black',
    },
    centerRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logo: {
        width: 30,
        height: 30,
        marginLeft: 5
    },
    text: {
        flex: 1,
        textAlign: 'left',
        left: 40,
        position: 'absolute'
    }
})

export default SheetIdentityCell;
