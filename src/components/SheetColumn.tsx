import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import SheetCell from './SheetCell';
import { BodyContent } from '../types';

interface SheetColumnProps {
    content: BodyContent[];
}

const SheetColumn: React.FC<SheetColumnProps> = ({content}) => {
    const [cells, setCells] = useState<any>();

    useEffect(() => {
        setCells(content.map(item => <SheetCell key={item.key} content={item.text}/>));
    }, [])


    return <View style={styles.column}>{cells}</View>
}

const styles = StyleSheet.create({
    column: {flexDirection: "column"},
})

export default SheetColumn;
