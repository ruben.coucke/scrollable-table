import React from 'react';
import { StyleSheet, Animated } from 'react-native';
import { CELL_HEIGHT, IdentityContent, CELL_WIDTH, IDENTITY_COLUMN_MIN_WIDTH } from '../types';
import SheetIdentityCell from './SheetIdentityCell';

interface SheetIdentityColumnProps {
    content: IdentityContent[];
    scrollPositionLeft: Animated.Value;
}

const SheetIdentityColumn: React.FC<SheetIdentityColumnProps> = ({content, scrollPositionLeft}) => {
    return (
        <Animated.View style={[styles.identity, {
            transform: [
                {
                    translateX: scrollPositionLeft.interpolate({
                        inputRange: [0, CELL_WIDTH],
                        outputRange: [0, -CELL_WIDTH + IDENTITY_COLUMN_MIN_WIDTH],
                        extrapolate: 'clamp'
                    })
                }
            ]
        }]}>
            {content.map(value => <SheetIdentityCell scrollPositionLeft={scrollPositionLeft} shrunkContent={value.shrunkText} expandedContent={value.expandedText} imageUrl={value.image}
                                                     key={value.key}/>)}
        </Animated.View>
    );
};

const styles = StyleSheet.create({
    identity: {position: "absolute", marginTop: CELL_HEIGHT + 2, backgroundColor: 'white', zIndex: 1}
})

export default SheetIdentityColumn;
