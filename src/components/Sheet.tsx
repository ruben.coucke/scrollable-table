import { Animated, ScrollView, View } from 'react-native';
import SheetBody from './SheetBody';
import React, { useEffect, useState } from 'react';
import { BodyContent, IdentityContent } from '../types';
import SheetHeader from './SheetHeader';
import SheetIdentityColumn from './SheetIdentityColumn';
import randomAlpabetic from '../randomAlpabetic';

interface SheetProps {
    headerContent: BodyContent[];
    bodyContent: BodyContent[][];
    leftSidebarContent: IdentityContent[];
}

const scrollPositionLeft = new Animated.Value(0);

const scrollEvent = Animated.event(
    [{nativeEvent: {contentOffset: {x: scrollPositionLeft}}}],
    {useNativeDriver: true},
);

const Sheet: React.FC<SheetProps> = ({leftSidebarContent, bodyContent, headerContent}) => {
    const [prependedBodyContent, setPrependedBodyContent] = useState<BodyContent[][]>([]);
    useEffect(() => {
        setPrependedBodyContent([Array.from({length: bodyContent.length}, () => ({key: randomAlpabetic(), text: ''})), ...bodyContent]);
    }, bodyContent);

    if (!prependedBodyContent) {
        return null;
    }

    return (
        <View>
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                bounces={false}
                stickyHeaderIndices={[0]}
            >
                <SheetHeader scrollPositionLeft={scrollPositionLeft} headerContent={headerContent} topLeftHeaderText={'Top left'}/>
                <SheetIdentityColumn content={leftSidebarContent} scrollPositionLeft={scrollPositionLeft}/>
                <SheetBody bodyContent={prependedBodyContent} scrollEvent={scrollEvent}/>
            </ScrollView>
        </View>
    );
};

export default Sheet;
