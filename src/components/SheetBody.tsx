import React from 'react';
import { Animated } from 'react-native';
import SheetColumn from './SheetColumn';
import { BodyContent, CELL_WIDTH } from '../types';

interface SheetBodyProps {
    bodyContent: BodyContent[][];
    scrollEvent: any;
}

const SheetBody: React.FC<SheetBodyProps> = ({bodyContent, scrollEvent}) => {
    return <Animated.FlatList
        bounces={false}
        horizontal
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item: BodyContent[]) => bodyContent.indexOf(item).toString()}
        data={bodyContent}
        renderItem={({item}: { item: BodyContent[] }) => <SheetColumn content={item}/>}
        onScroll={scrollEvent}
        scrollEventThrottle={16}
        snapToOffsets={[0, CELL_WIDTH]}
        snapToEnd={false}
    />;
};

export default SheetBody;
