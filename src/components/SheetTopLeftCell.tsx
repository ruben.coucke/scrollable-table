import { Animated, Text, StyleSheet, ViewProps } from 'react-native';
import React from 'react';
import { CELL_HEIGHT, CELL_WIDTH, IDENTITY_COLUMN_MIN_WIDTH } from '../types';

interface SheetTopLeftCellProps extends ViewProps {
    content: string;
    scrollPositionLeft: Animated.Value;
}

const SheetTopLeftCell: React.FC<SheetTopLeftCellProps> = ({content, scrollPositionLeft}) => {
    return (
        <Animated.View style={[styles.cell, styles.centerRow]}>
            <Animated.View style={[styles.centerRow, {
                transform: [
                    {
                        translateX: scrollPositionLeft.interpolate({
                            inputRange: [0, CELL_WIDTH],
                            outputRange: [1, CELL_WIDTH - IDENTITY_COLUMN_MIN_WIDTH + 1],
                            extrapolate: 'clamp'
                        })
                    }
                ]
            }]}>
                <Text
                    numberOfLines={1}
                    style={styles.text}>
                    {content}
                </Text>
            </Animated.View>
        </Animated.View>
    )
};

const styles = StyleSheet.create({
    cell: {
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'black',
    },
    centerRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logo: {
        width: 30,
        height: 30,
        marginLeft: 5
    },
    text: {
        flex: 1,
        textAlign: 'left',
        marginLeft: 5
    }
})

export default SheetTopLeftCell;
