import { StyleSheet, ViewProps, Text, View } from 'react-native';
import React from 'react';
import { CELL_HEIGHT, CELL_WIDTH } from '../types';

interface SheetCellProps extends ViewProps {
    content: string;
}

const SheetCell: React.FC<SheetCellProps> = ({content}) => {
    return (
        <View style={[styles.cell, styles.centerRow]}>
            <Text
                numberOfLines={1}
                style={styles.text}>
                {content}
            </Text>
        </View>
    )
};

const styles = StyleSheet.create({
    cell: {
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderColor: 'black',
    },
    centerRow: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logo: {
        width: 30,
        height: 30,
        marginLeft: 5
    },
    text: {
        flex: 1,
        textAlign: 'center',
    }
})

export default SheetCell;
