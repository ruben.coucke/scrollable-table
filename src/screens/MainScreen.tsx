import React from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import Sheet from '../components/Sheet';
import randomAlpabetic from '../randomAlpabetic';
import { BodyContent, IdentityContent } from '../types';

export const NUM_COLS = 15
export const NUM_ROWS_STEP = 30

const headerContent: BodyContent[] = Array.from({length: NUM_COLS}, (any, index) =>
    ({
        text: `Header #${index + 1}`,
        key: randomAlpabetic()
    })
);
const leftSidebarContent: IdentityContent[] = Array.from({length: NUM_ROWS_STEP}, (any, index) =>
    ({
        expandedText: `Sidebar #${index + 1}`,
        shrunkText: `#${index + 1}`,
        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/f/f9/Royal_Belgian_FA_logo_2019.svg/1200px-Royal_Belgian_FA_logo_2019.svg.png',
        key: randomAlpabetic()
    })
);
const bodyContent: BodyContent[][] = Array.from({length: NUM_COLS}, (col, colIndex) =>
    Array.from({length: NUM_ROWS_STEP}, (row, rowIndex) =>
        ({
            text: `Row: ${rowIndex + 1}- Col:${colIndex + 1}`,
            key: randomAlpabetic()
        })
    )
);

const MainScreen = () => {
    return (
        <SafeAreaView>
            <View style={styles.container}>
                <Sheet leftSidebarContent={leftSidebarContent} bodyContent={bodyContent} headerContent={headerContent}/>
            </View>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20,
        borderColor: 'black',
        borderLeftWidth: 2,
        borderRightWidth: 2,
        height: 500,
        marginTop: 100
    }
})

export default MainScreen;
